# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy import Field


class DataExtractItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass


class EscolaItem(scrapy.Item):
    name = Field()
    site = Field()
    endereco = Field()
    telefone = Field()
    nota = Field()
    uf = Field()
    municipio = Field()
