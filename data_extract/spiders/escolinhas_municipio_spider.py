import scrapy

from data_extract.items import EscolaItem


class EscolinhasMunicipioSpider(scrapy.Spider):
    name = 'EscolinhasMunicipioSpider'
    BASE_URL = 'https://www.google.com'

    def __init__(self, municipio='florianopolis', uf='SC'):
        self.municipio = municipio
        self.uf = uf

    def start_requests(self):
        url = self.BASE_URL+'/search?q=escolinhas+de+futebol+em+{municipio}'.format(municipio=self.municipio)
        yield scrapy.Request(url=url, callback=self.get_mais_lugares)

    def get_mais_lugares(self, response):
        paramters_url = response.xpath('//a[contains(., "Mais lugares")]/@href').extract_first()
        yield scrapy.Request(url=self.BASE_URL+paramters_url, callback=self.get_lista_escolinhas)

    def get_lista_escolinhas(self, response):
        mais_detalhes_href = response.xpath('//div[table//a/div/span]//a[contains(., "Mais informa")]/@href').extract()
        for prefix_url in mais_detalhes_href:
            yield scrapy.Request(url=self.BASE_URL+prefix_url, callback=self.parse)

        for next_page in response.xpath("//a[span[contains(@class, 'csb')]]"):
            print('##################################')
            print(self.BASE_URL+next_page.xpath('@href').extract_first())
            print('##################################')
            yield scrapy.Request(url=self.BASE_URL+next_page.xpath('@href').extract_first(), callback=self.get_lista_escolinhas_paginadas)

    def get_lista_escolinhas_paginadas(self, response):
        mais_detalhes_href = response.xpath('//div[table//a/div/span]//a[contains(., "Mais informa")]/@href').extract()
        for prefix_url in mais_detalhes_href:
            yield scrapy.Request(url=self.BASE_URL+prefix_url, callback=self.parse)

    def parse(self, response):
        item = EscolaItem()
        item['name'] = response.xpath('//*[@id="rhs_block"]/ol/div/div/div[2]/div/div/text()').extract_first()
        item['site'] = response.xpath('//*[@id="rhs_block"]//a[contains(., "Site")]/@href').extract_first()
        item['endereco'] = response.xpath('//*[@id="rhs_block"]//div[span[contains(., "Endere")]]/span[2]/text()').extract_first()
        item['telefone'] = response.xpath('//*[@id="rhs_block"]//div[span[contains(., "Telefone")]]/span[2]/text()').extract_first()
        item['nota'] = self.get_nota(response.xpath('//*[@id="rhs_block"]/ol/div/div/div[3]/div/span[1]/text()').extract_first())
        item['municipio'] = self.municipio
        item['uf'] = self.uf
        yield item

    def get_nota(self, nota):
        try:
            return float(nota.replace(',', '.'))
        except Exception:
            return -1.0
