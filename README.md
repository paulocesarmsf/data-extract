# data-extract

### Pre Requisites 

- MongoDB
- Docker command for run:
    
    docker run -d -p 27017:27017 -p 28017:28017 -e AUTH=no tutum/mongodb
    
    docker run -d --name mongodb -p 27017:27017 -e AUTH=no -v $HOME/Documentos/projetos/projeto-futebol/mongodatabase:/data/db tutum/mongodb

### Run
scrapy crawl EscolinhasMunicipioSpider -a municipio={municipio} -a uf={uf} -o {municipio}.csv